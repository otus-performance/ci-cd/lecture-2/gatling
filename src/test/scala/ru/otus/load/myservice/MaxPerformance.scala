package ru.otus.load.myservice

import io.gatling.core.Predef._
import org.galaxio.gatling.config.SimulationConfig._
import ru.otus.load.myservice.scenarios.CommonScenario

class MaxPerformance extends Simulation {

  setUp(
    CommonScenario().inject(
      incrementUsersPerSec(intensity / stagesNumber)
        .times(stagesNumber)
        .eachLevelLasting(stageDuration)
        .separatedByRampsLasting(rampDuration)
        .startingFrom(0)
    )
  ).protocols(httpProtocol)
    .maxDuration(testDuration)

}
