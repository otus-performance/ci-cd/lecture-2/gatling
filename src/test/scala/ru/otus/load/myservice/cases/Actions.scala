package ru.otus.load.myservice.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._

object Actions {

  val getComputers = http("GET /computers")
    .get("/computers")
    .check(status is 200)

  val getNewComputers = http("GET /computers/new")
    .get("/computers/new")
    .check(status is 200)

}
