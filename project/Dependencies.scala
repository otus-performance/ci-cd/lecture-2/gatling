import sbt._

object Dependencies {
  lazy val gatling: Seq[ModuleID] = Seq(
    "io.gatling.highcharts" % "gatling-charts-highcharts",
    "io.gatling"            % "gatling-test-framework"
  ).map(_ % "3.13.1" % Test)

  lazy val gatlingPicatinny: Seq[ModuleID] = Seq(
    "org.galaxio" %% "gatling-picatinny"
  ).map(_ % "0.16.1")
}
